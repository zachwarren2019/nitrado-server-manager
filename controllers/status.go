package controllers

import (
	"net/http"

	"gitlab.com/BIC_Dev/nitrado-server-manager/viewmodels"
)

// GetStatus responds with the availability status of this service
func (c *Controller) GetStatus(w http.ResponseWriter, r *http.Request) {
	status := viewmodels.GetStatusResponse{
		Status:  "success",
		Message: "Service is available",
	}

	SendJSONResponse(w, status, http.StatusOK)
}

// GetGuildStatus responds with all currently loaded guilds
func (c *Controller) GetGuildStatus(w http.ResponseWriter, r *http.Request) {
	var guilds []viewmodels.GetGuildStatusGuilds

	for guildID, guild := range c.GuildConfig.Guilds {
		guilds = append(guilds, viewmodels.GetGuildStatusGuilds{
			ID:          guildID,
			Name:        guild.Guild.Name,
			ContactID:   guild.Guild.ContactID,
			ContactName: guild.Guild.ContactName,
		})
	}

	SendJSONResponse(w, viewmodels.GetGuildStatusResponse{
		Status: "success",
		Guilds: guilds,
	}, http.StatusOK)
}
