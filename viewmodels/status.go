package viewmodels

// GetStatusResponse struct
type GetStatusResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

// GetGuildStatusResponse struct
type GetGuildStatusResponse struct {
	Status string                 `json:"status"`
	Guilds []GetGuildStatusGuilds `json:"guilds"`
}

// GetGuildStatusGuilds struct
type GetGuildStatusGuilds struct {
	ID          int64  `json:"id"`
	Name        string `json:"name"`
	ContactID   int64  `json:"contact_id"`
	ContactName string `json:"contact_name"`
}
