package viewmodels

// UpdateAllGuildsResponse struct
type UpdateAllGuildsResponse struct {
	Status        string             `json:"status"`
	Message       string             `json:"message"`
	GuildsUpdated []int64            `json:"guilds_updated"`
	GuildsFailed  []GuildsFailedInfo `json:"guilds_failed"`
}

// UpdateGuildsResponse struct
type UpdateGuildsResponse struct {
	Status        string             `json:"status"`
	Message       string             `json:"message"`
	GuildsUpdated []int64            `json:"guilds_updated"`
	GuildsFailed  []GuildsFailedInfo `json:"guilds_failed"`
}

// GuildsFailedInfo struct
type GuildsFailedInfo struct {
	GuildID int64  `json:"guild_id"`
	Error   string `json:"error"`
	Message string `json:"message"`
}

// UpdateGuildsRequestBody struct
type UpdateGuildsRequestBody struct {
	GuildIDs []int64 `json:"guild_ids"`
}
