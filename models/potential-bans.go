package models

import (
	"net/http"

	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils/db"
)

// PotentialBan struct
type PotentialBan struct {
	MessageID     string `gorm:"varchar(50);index"`
	PlayerID      string `gorm:"varchar(50)"`
	PlayerName    string `gorm:"varchar(50)"`
	DiscordUserID string `gorm:"varchar(50)"`
	GameserverID  int
	gorm.Model
}

// TableName func
func (PotentialBan) TableName() string {
	return "potential_ban"
}

// Create adds a record to DB
func (t *PotentialBan) Create(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Create(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to create potential ban entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

// GetLatest gets the latest record for a GameserverID
func (t *PotentialBan) GetLatest(DBStruct db.Interface) (PotentialBan, *utils.ModelError) {
	var potentialBan PotentialBan

	result := DBStruct.GetDB().Where("message_id = ? AND discord_user_id = ?", t.MessageID, t.DiscordUserID).Last(&potentialBan)

	if gorm.IsRecordNotFoundError(result.Error) {
		return potentialBan, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find potential ban by message_id and discord_user_id in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return PotentialBan{}, modelError
	}

	return potentialBan, nil
}

// Delete soft deletes a potential ban in DB
func (t *PotentialBan) Delete(DBStruct db.Interface) *utils.ModelError {

	result := DBStruct.GetDB().Delete(&t)

	if gorm.IsRecordNotFoundError(result.Error) {
		return nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to delete potential ban in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}
