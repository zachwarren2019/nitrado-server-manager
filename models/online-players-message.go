package models

import (
	"fmt"
	"net/http"

	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils/db"
)

// OnlinePlayersMessage model
type OnlinePlayersMessage struct {
	gorm.Model
	DiscordGuild     int64
	DiscordChannel   int64
	GameserverID     int
	DiscordMessageID int64
}

// TableName func
func (OnlinePlayersMessage) TableName() string {
	return "online_players_message"
}

// Create adds a record to DB
func (t *OnlinePlayersMessage) Create(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Create(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to create online players entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

// GetLatest gets the latest record for a GameserverID
func (t *OnlinePlayersMessage) GetLatest(DBStruct db.Interface) (OnlinePlayersMessage, *utils.ModelError) {
	var onlinePlayersMessage OnlinePlayersMessage

	result := DBStruct.GetDB().Where("discord_guild = ? AND discord_channel = ? AND gameserver_id = ?", t.DiscordGuild, t.DiscordChannel, t.GameserverID).Last(&onlinePlayersMessage)

	if gorm.IsRecordNotFoundError(result.Error) {
		return onlinePlayersMessage, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find online players message entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return OnlinePlayersMessage{}, modelError
	}

	return onlinePlayersMessage, nil
}

// Update updates a record or creates it if it doesn't exist
func (t *OnlinePlayersMessage) Update(DBStruct db.Interface) *utils.ModelError {
	getResult, getErr := t.GetLatest(DBStruct)

	if getErr != nil {
		return getErr
	}

	if getResult.ID == 0 {
		createErr := t.Create(DBStruct)

		if createErr != nil {
			return createErr
		}

		return nil
	}

	result := DBStruct.GetDB().Model(&t).Where("discord_guild = ? AND discord_channel = ? AND gameserver_id = ?", t.DiscordGuild, t.DiscordChannel, t.GameserverID).Updates(map[string]interface{}{
		"discord_message_id": t.DiscordMessageID,
	})

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage(fmt.Sprintf("Unable to update online players entry in DB: %+v", &t))
		modelError.SetStatus(http.StatusInternalServerError)
		return modelError
	}

	return nil
}
