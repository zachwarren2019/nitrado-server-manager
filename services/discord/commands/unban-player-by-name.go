package commands

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/discord/discordapi"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/filestore"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/nitrado"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils/db"
)

// UnbanPlayer struct
type UnbanPlayer struct {
	PlayerName string
	PlayerID   string
	Servers    []string
}

// UnbanPlayerError struct
type UnbanPlayerError struct {
	Errs        []*utils.ServiceError
	Gameservers []filestore.Gameserver
}

const minLengthUnbanPlayerName = 3

// UnbanPlayerByNameCommand func
func (ch *CommandHandler) UnbanPlayerByNameCommand(command utils.Command, message *discordgo.MessageCreate, args []string) {
	ch.Log.Log(fmt.Sprintf("COMMAND: Unban player: %s", message.Content), ch.Log.LogInformation)

	if len(args) < 1 {
		ch.SendErrorMessage("ERROR: Unban Player", "Invalid playername", fmt.Sprintf("Must include %d or more characters", minLengthBanPlayerName), "", message.Content, message.ChannelID)
		return
	}

	var singleUnbanGameserver *filestore.Gameserver
	potentialGameserverID, pe := strconv.Atoi(args[0])

	if pe == nil {
		for _, gameserver := range ch.Guild.Gameservers {
			if gameserver.ID == potentialGameserverID {
				singleUnbanGameserver = &gameserver
				break
			}
		}
	}

	var playerName string

	if singleUnbanGameserver != nil {
		if len(args) < 2 {
			ch.SendErrorMessage("ERROR: Unban Player", "Missing player name", fmt.Sprintf("Must include player name with %d or more characters", minLengthBanPlayerName), "", message.Content, message.ChannelID)
			return
		}

		playerName = strings.Join(args[1:], " ")
	} else if len(args[0]) > 5 && pe == nil {
		ch.SendErrorMessage("ERROR: Unban Player", "Invalid gameserver ID", fmt.Sprintf("Please use the `%s%s` command to get the gameserver ID", ch.Guild.Bot.Prefix, ch.Config.Bot.Commands["servers"].Name), "", message.Content, message.ChannelID)
		return
	} else {
		playerName = strings.Join(args, " ")
	}

	if len(playerName) < minLengthUnbanPlayerName {
		ch.SendErrorMessage("ERROR: Unban Player", "Invalid search", fmt.Sprintf("Must include %d or more characters", minLengthSearchPlayerName), "", message.Content, message.ChannelID)
		return
	}

	request := nitrado.NitradoRequest{
		Config:                ch.Config,
		NitradoV2ServiceToken: ch.NitradoV2ServiceToken,
	}

	var requestData []nitrado.RequestData
	params := map[string]string{
		"online":      "false",
		"exact-match": "true",
	}

	for _, gameserver := range ch.Guild.Gameservers {
		requestData = append(requestData, nitrado.RequestData{
			Command:    command,
			Player:     nitrado.Player{Name: playerName},
			Gameserver: gameserver,
		})
	}

	responses, errors := request.GoRequest(request.SearchPlayersByName, requestData, params)

	// if len(responses) == 0 {
	// 	ch.SendErrorMessage("Unban Player", "No players found", "", "", message.Content, message.ChannelID)
	// 	return
	// }

	sp := ParsePlayersFromRequestResponse(responses)

	var embeddablePlayers []EmbeddableField

	for _, player := range sp {
		embeddablePlayers = append(embeddablePlayers, player)
	}

	var spe SearchPlayerError

	for _, err := range errors {
		if err.Error.StatusCode == http.StatusNotFound {
			continue
		}

		spe.Errs = append(spe.Errs, err.Error)
		spe.Gameservers = append(spe.Gameservers, err.Gameserver)
	}

	var embeddableErrors []EmbeddableError

	if len(spe.Errs) > 0 {
		embeddableErrors = append(embeddableErrors, &spe)
	}

	description := fmt.Sprintf("**Command:** `%s`", message.Content)

	if len(embeddablePlayers) > 0 {
		if singleUnbanGameserver != nil {
			description += fmt.Sprintf("\nClick on the %s reaction to **unban** the player on %s", ch.Command.Reactions["unban_player"].Icon, singleUnbanGameserver.Name)
		} else {
			description += fmt.Sprintf("\nClick on the %s reaction to **unban** the player", ch.Command.Reactions["unban_player"].Icon)
		}

	} else {
		description += "\n**No player found**"
	}

	embedParams := EmbeddableParams{
		Title:       "Unban Player by Name",
		Description: description,
		Color:       ch.Guild.Bot.Commands["unban"].Color,
		TitleURL:    ch.Config.Bot.TitleURL,
	}

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	embed := CreateEmbed(embedParams, embeddablePlayers, embeddableErrors)

	// Can get message ID from response here
	postedMessage, cpErr := discord.CreatePost(message.ChannelID, "", embed)

	if cpErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: Failed to post unban player: %s", cpErr.Error()), ch.Log.LogMedium)
	}

	if len(embeddablePlayers) == 0 {
		return
	}

	prErr := ch.PostReaction(postedMessage.ChannelID, postedMessage.ID, ch.Command.Reactions["unban_player"].Icon)

	if prErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", prErr.GetMessage(), prErr.Error()), ch.Log.LogMedium)
		return
	}

	var mErr *utils.ModelError

	if singleUnbanGameserver != nil {
		mErr = AddPotentialUnbanToDB(ch.Config, postedMessage.ID, message.Author.ID, sp, singleUnbanGameserver.ID)
	} else {
		mErr = AddPotentialUnbanToDB(ch.Config, postedMessage.ID, message.Author.ID, sp, 0)
	}

	if mErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", mErr.GetMessage(), mErr.Error()), ch.Log.LogMedium)
	}
}

// HandleUnbanReaction func
func (ch *CommandHandler) HandleUnbanReaction(command utils.Command, reaction *discordgo.MessageReactionAdd) {
	unban, gErr := GetPotentialUnbanFromDB(ch.Config, reaction.MessageID, reaction.UserID)

	if gErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", gErr.GetMessage(), gErr.Error()), ch.Log.LogMedium)
		return
	}

	if unban.PlayerID == "" {
		ch.Log.Log("ERROR: No potential unban found", ch.Log.LogInformation)
		return
	}

	if unban.DiscordUserID != reaction.UserID {
		ch.Log.Log("ERROR: Wrong discord user tried to unban user", ch.Log.LogInformation)
	}

	request := nitrado.NitradoRequest{
		Config:                ch.Config,
		NitradoV2ServiceToken: ch.NitradoV2ServiceToken,
	}

	var requestData []nitrado.RequestData

	for _, gameserver := range ch.Guild.Gameservers {
		if unban.GameserverID == 0 {
			requestData = append(requestData, nitrado.RequestData{
				Command:    command,
				Player:     nitrado.Player{ID: unban.PlayerID},
				Gameserver: gameserver,
			})
		} else if unban.GameserverID == gameserver.ID {
			requestData = append(requestData, nitrado.RequestData{
				Command:    command,
				Player:     nitrado.Player{ID: unban.PlayerID},
				Gameserver: gameserver,
			})
		}
	}

	responses, errors := request.GoRequest(request.UnbanPlayer, requestData, nil)

	ub := ParseGameserversFromUnbanRequestResponse(responses)

	var embeddableFields []EmbeddableField

	if ub != nil {
		ub.PlayerID = unban.PlayerID
		ub.PlayerName = unban.PlayerName
		embeddableFields = append(embeddableFields, ub)
	}

	var ube UnbanPlayerError

	for _, err := range errors {
		// Ignore 404 errors
		if err.Error.StatusCode == http.StatusNotFound {
			continue
		}

		ube.Errs = append(ube.Errs, err.Error)
		ube.Gameservers = append(ube.Gameservers, err.Gameserver)
	}

	var embeddableErrors []EmbeddableError

	if len(ube.Errs) > 0 {
		embeddableErrors = append(embeddableErrors, &ube)
	}

	embedParams := EmbeddableParams{
		Title:       "Unban Player by Name",
		Description: fmt.Sprintf("**Unbanned:** `%s`\nThis will be applied when Nitrado updates Ark with the new unban. It may take some time.", ub.PlayerName),
		Color:       ch.Guild.Bot.Commands["unban"].Color,
		TitleURL:    ch.Config.Bot.TitleURL,
	}

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	embed := CreateEmbed(embedParams, embeddableFields, embeddableErrors)

	// Can get message ID from response here
	_, cpErr := discord.CreatePost(reaction.ChannelID, "", embed)

	if cpErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: Failed to post unban player message: %s", cpErr.Error()), ch.Log.LogMedium)
	}

	dErr := DeletePotentialUnbanFromDB(ch.Config, unban)

	if dErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", dErr.GetMessage(), dErr.Error()), ch.Log.LogMedium)
	}
}

// ConvertToEmbedField func
func (sp *UnbanPlayer) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	var servers string

	for _, server := range sp.Servers {
		servers = servers + "\n\t" + server
	}

	fieldVal := fmt.Sprintf("```Player ID: %s\nServers: %s```", sp.PlayerID, servers)

	field := &discordgo.MessageEmbedField{
		Name:   sp.PlayerName,
		Value:  fieldVal,
		Inline: false,
	}

	return field, nil
}

// ConvertToEmbedField func
func (spe *UnbanPlayerError) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	var gameservers []string

	for _, gs := range spe.Gameservers {
		gameservers = append(gameservers, gs.Name)
	}

	field := &discordgo.MessageEmbedField{
		Name:   "ERROR: Failed to unban player on gameservers",
		Value:  fmt.Sprintf("```%s```", strings.Join(gameservers, "\n")),
		Inline: false,
	}

	return field, nil
}

// ParseGameserversFromUnbanRequestResponse func
func ParseGameserversFromUnbanRequestResponse(rr []*nitrado.RequestResponse) *UnbanPlayer {
	var ubp UnbanPlayer

	for _, gs := range rr {
		ubp.Servers = append(ubp.Servers, gs.Gameserver.Name)
	}

	return &ubp
}

// AddPotentialUnbanToDB func
func AddPotentialUnbanToDB(config *utils.Config, messageID string, userID string, players map[string]*SearchPlayer, gameserverID int) *utils.ModelError {
	dbInt, dbErr := db.GetDB(config)

	if dbErr != nil {
		return dbErr
	}

	defer dbInt.GetDB().Close()

	for _, player := range players {
		potentialBan := &models.PotentialUnban{
			MessageID:     messageID,
			PlayerID:      player.ID,
			PlayerName:    player.Name,
			DiscordUserID: userID,
			GameserverID:  gameserverID,
		}

		mErr := potentialBan.Create(dbInt)

		if mErr != nil {
			return mErr
		}
	}

	return nil
}

// GetPotentialUnbanFromDB func
func GetPotentialUnbanFromDB(config *utils.Config, messageID string, userID string) (*models.PotentialUnban, *utils.ModelError) {
	dbInt, dbErr := db.GetDB(config)

	if dbErr != nil {
		return nil, dbErr
	}

	defer dbInt.GetDB().Close()

	potentialUnban := &models.PotentialUnban{
		MessageID:     messageID,
		DiscordUserID: userID,
	}

	dbBan, mErr := potentialUnban.GetLatest(dbInt)

	if mErr != nil {
		return nil, mErr
	}

	return &dbBan, nil
}

// DeletePotentialUnbanFromDB func
func DeletePotentialUnbanFromDB(config *utils.Config, unban *models.PotentialUnban) *utils.ModelError {
	dbInt, dbErr := db.GetDB(config)

	if dbErr != nil {
		return nil
	}

	defer dbInt.GetDB().Close()

	mErr := unban.Delete(dbInt)

	if mErr != nil {
		return mErr
	}

	return nil
}
