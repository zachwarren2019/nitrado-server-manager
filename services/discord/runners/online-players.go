package runners

import (
	"fmt"
	"math"
	"strconv"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/gammazero/workerpool"
	"gitlab.com/BIC_Dev/nitrado-server-manager/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/discord/commands"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/discord/discordapi"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/filestore"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/nitrado"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils/db"
)

// Players struct
type Players struct {
	Players    []nitrado.Player
	GuildID    int64
	Gameserver *filestore.Gameserver
}

// OnlinePlayers struct
type OnlinePlayers struct {
	Players    []nitrado.Player
	Gameserver *filestore.Gameserver
}

// OnlinePlayersError struct
type OnlinePlayersError struct {
	Errs        []*utils.ServiceError
	Gameservers []filestore.Gameserver
}

// OnlinePlayersRunner func
func (r *Runner) OnlinePlayersRunner() {
	r.Log.Log("PROCESS: Online players runner started", r.Log.LogInformation)

	ticker := time.NewTicker(time.Duration(r.Config.Runners.PlayersInterval) * time.Second)

	wp := workerpool.New(r.Config.Runners.PlayersWorkers)

	for range ticker.C {
		for _, guild := range r.GuildConfig.Guilds {
			r.Log.Log(fmt.Sprintf("PROCESS: Running online players for guild: %d", guild.Guild.ID), r.Log.LogInformation)
			for _, gameserver := range guild.Gameservers {
				if !gameserver.PlayerList.Enabled || gameserver.PlayerList.ChannelID == 0 {
					continue
				}

				r.AddOnlinePlayersJobToWorkerPool(wp, guild, gameserver)
			}
		}
	}
}

// AddOnlinePlayersJobToWorkerPool func
func (r *Runner) AddOnlinePlayersJobToWorkerPool(wp *workerpool.WorkerPool, guild filestore.Guild, gameserver filestore.Gameserver) {
	wp.Submit(func() {
		r.OnlinePlayersJob(guild, gameserver)
	})
}

// OnlinePlayersJob func
func (r *Runner) OnlinePlayersJob(guild filestore.Guild, gameserver filestore.Gameserver) {
	r.Log.Log(fmt.Sprintf("PROCESS: Getting online players for gameserver: %d", gameserver.ID), r.Log.LogInformation)

	request := nitrado.NitradoRequest{
		Config:                r.Config,
		NitradoV2ServiceToken: r.NitradoV2ServiceToken,
	}

	var requestData []nitrado.RequestData
	params := map[string]string{
		"online":        "true",
		"exact-match":   "false",
		"cache-control": "no-cache",
	}

	requestData = append(requestData, nitrado.RequestData{
		Gameserver: gameserver,
	})

	responses, errors := request.GoRequest(request.GetAllPlayers, requestData, params)

	for _, err := range errors {
		r.Log.Log(fmt.Sprintf("ERROR: %s: %s", err.Error.Message, err.Error.Error()), r.Log.LogLow)
	}

	var embeddableOnlinePlayers []commands.EmbeddableField

	for _, resp := range responses {
		if resp.Players == nil {
			continue
		}

		var onlinePlayers []OnlinePlayers
		var count int = 0
		var playersLen int = len(resp.Players)

		for i := 0; i < playersLen; i++ {
			if len(onlinePlayers) <= count {
				onlinePlayers = append(onlinePlayers, OnlinePlayers{})
			}

			onlinePlayers[count].Players = append(onlinePlayers[count].Players, resp.Players[i])

			if math.Mod(float64(i+1), 40.0) == 0 {
				embeddableOnlinePlayers = append(embeddableOnlinePlayers, &onlinePlayers[count])
				count++
			} else if i+1 >= playersLen {
				embeddableOnlinePlayers = append(embeddableOnlinePlayers, &onlinePlayers[count])
			}
		}
	}

	var ope OnlinePlayersError

	for _, err := range errors {
		ope.Errs = append(ope.Errs, err.Error)
		ope.Gameservers = append(ope.Gameservers, err.Gameserver)
	}

	var embeddableErrors []commands.EmbeddableError

	if len(ope.Errs) > 0 {
		embeddableErrors = append(embeddableErrors, &ope)
	}

	embedParams := commands.EmbeddableParams{
		Title:       fmt.Sprintf("Online Players: %s", gameserver.Name),
		Description: fmt.Sprintf("Online players updated every %d seconds", r.Config.Runners.PlayersInterval),
		Color:       0x17D34B,
		TitleURL:    r.Config.Bot.TitleURL,
		Footer:      "Updated",
	}

	embed := commands.CreateEmbed(embedParams, embeddableOnlinePlayers, embeddableErrors)

	onlinePlayersMessage, opErr := getOnlinePlayersMessage(guild.Guild.ID, gameserver.PlayerList.ChannelID, gameserver.ID, r.Config)

	if opErr != nil {
		r.Log.Log(fmt.Sprintf("ERROR: %s: %s", opErr.GetMessage(), opErr.Error()), r.Log.LogMedium)
		return
	}

	if onlinePlayersMessage.ID == 0 {
		go r.CreateOnlinePlayersPost("", embed, onlinePlayersMessage.DiscordMessageID, guild.Guild.ID, gameserver)
	} else {
		go r.UpdateOnlinePlayersPost("", embed, onlinePlayersMessage.DiscordMessageID, guild.Guild.ID, gameserver)
	}

	return
}

// ConvertToEmbedField func
func (op *OnlinePlayers) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	var fieldVal string = ""

	for _, player := range op.Players {
		fieldVal += "🟢 " + player.Name + "\n\n"
	}

	field := &discordgo.MessageEmbedField{
		Name:   "\u200b",
		Value:  fieldVal,
		Inline: false,
	}

	return field, nil
}

// ConvertToEmbedField func
func (ope *OnlinePlayersError) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	var gameservers []string

	for _, gs := range ope.Gameservers {
		gameservers = append(gameservers, gs.Name)
	}

	var errs string

	for _, err := range ope.Errs {
		errs = fmt.Sprintf("%s\n%s", errs, err.Error())
	}

	field := &discordgo.MessageEmbedField{
		Name:   "ERROR: Failed Online Players Lookup",
		Value:  errs,
		Inline: false,
	}

	return field, nil
}

func getOnlinePlayersMessage(guildID int64, channelID int64, gameserverID int, c *utils.Config) (*models.OnlinePlayersMessage, *utils.ModelError) {
	dbInterface, err := db.GetDB(c)

	if err != nil {
		return nil, err
	}

	defer dbInterface.GetDB().Close()

	onlinePlayersMessage := models.OnlinePlayersMessage{
		DiscordGuild:   guildID,
		DiscordChannel: channelID,
		GameserverID:   gameserverID,
	}

	dbStatusMessage, dbErr := onlinePlayersMessage.GetLatest(dbInterface)

	if dbErr != nil {
		return nil, dbErr
	}

	return &dbStatusMessage, nil
}

func saveOnlinePlayersMessage(guildID int64, channelID int64, gameserverID int, discordMessageID int64, c *utils.Config) *utils.ModelError {
	dbInterface, err := db.GetDB(c)

	if err != nil {
		return err
	}

	defer dbInterface.GetDB().Close()

	onlinePlayersMessage := models.OnlinePlayersMessage{
		DiscordGuild:     guildID,
		DiscordChannel:   channelID,
		DiscordMessageID: discordMessageID,
		GameserverID:     gameserverID,
	}

	dbErr := onlinePlayersMessage.Update(dbInterface)

	return dbErr
}

// CreateOnlinePlayersPost func
func (r *Runner) CreateOnlinePlayersPost(content string, embed *discordgo.MessageEmbed, prevDiscordMessageID int64, guildID int64, gameserver filestore.Gameserver) {
	discord := discordapi.Discord{
		DG:  r.DG,
		Log: r.Log,
	}

	channelID := fmt.Sprintf("%d", gameserver.PlayerList.ChannelID)

	message, cpErr := discord.CreatePost(channelID, "", embed)

	if cpErr != nil {
		r.Log.Log(fmt.Sprintf("ERROR: Failed to post online players for gameserver (%d): %s", gameserver.ID, cpErr.Error()), r.Log.LogLow)
		return
	}

	messageID, piErr := strconv.ParseInt(message.ID, 10, 64)

	if piErr != nil {
		r.Log.Log(fmt.Sprintf("ERROR: Failed to parse message ID into int64 (%s): %s", message.ID, piErr.Error()), r.Log.LogHigh)
		return
	}

	if messageID != prevDiscordMessageID {
		sErr := saveOnlinePlayersMessage(guildID, gameserver.PlayerList.ChannelID, gameserver.ID, messageID, r.Config)

		if sErr != nil {
			r.Log.Log(fmt.Sprintf("ERROR: %s for gameserver (%d): %s", sErr.GetMessage(), gameserver.ID, sErr.Error()), r.Log.LogHigh)
			return
		}
	}

	return
}

// UpdateOnlinePlayersPost func
func (r *Runner) UpdateOnlinePlayersPost(content string, embed *discordgo.MessageEmbed, prevDiscordMessageID int64, guildID int64, gameserver filestore.Gameserver) {
	discord := discordapi.Discord{
		DG:  r.DG,
		Log: r.Log,
	}

	channelID := fmt.Sprintf("%d", gameserver.PlayerList.ChannelID)

	var message *discordgo.Message
	var cpErr *utils.ServiceError
	var upErr *utils.ServiceError

	prevMessageIDString := fmt.Sprintf("%d", prevDiscordMessageID)
	message, upErr = discord.UpdatePost(channelID, prevMessageIDString, "", embed)

	if upErr != nil {
		if upErr.GetStatus() == 10008 {
			r.Log.Log(fmt.Sprintf("PROCESS: Creating new status post for gameserver (%d)", gameserver.ID), r.Log.LogInformation)
			message, cpErr = discord.CreatePost(channelID, "", embed)
		} else {
			r.Log.Log(fmt.Sprintf("ERROR: Failed to update online players for gameserver (%d): %s", gameserver.ID, upErr.Error()), r.Log.LogLow)
			return
		}
	}

	if cpErr != nil {
		r.Log.Log(fmt.Sprintf("ERROR: Failed to post online players for gameserver (%d): %s", gameserver.ID, cpErr.Error()), r.Log.LogLow)
		return
	}

	messageID, piErr := strconv.ParseInt(message.ID, 10, 64)

	if piErr != nil {
		r.Log.Log(fmt.Sprintf("ERROR: Failed to parse message ID into int64 (%s): %s", message.ID, piErr.Error()), r.Log.LogHigh)
		return
	}

	if messageID != prevDiscordMessageID {
		sErr := saveOnlinePlayersMessage(guildID, gameserver.PlayerList.ChannelID, gameserver.ID, messageID, r.Config)

		if sErr != nil {
			r.Log.Log(fmt.Sprintf("ERROR: %s for gameserver (%d): %s", sErr.GetMessage(), gameserver.ID, sErr.Error()), r.Log.LogHigh)
			return
		}
	}

	return
}
