package nitrado

import (
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/filestore"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
)

// NitradoRequest struct
type NitradoRequest struct {
	Config                *utils.Config
	NitradoV2ServiceToken string
}

// RequestData struct
type RequestData struct {
	Command    utils.Command
	Player     Player
	Gameserver filestore.Gameserver
}

// RequestResponse struct
type RequestResponse struct {
	Status     string
	Message    string
	Players    []Player
	PlayerLogs []PlayerLog
	AdminLogs  []AdminLog
	Gameserver filestore.Gameserver
}

// RequestError struct
type RequestError struct {
	Gameserver filestore.Gameserver
	Error      *utils.ServiceError
}

// GoRequest func
func (n *NitradoRequest) GoRequest(f func(RequestData, map[string]string, chan *RequestResponse, chan *RequestError), rd []RequestData, params map[string]string) ([]*RequestResponse, []*RequestError) {
	var response chan *RequestResponse = make(chan *RequestResponse, 100)
	var err chan *RequestError = make(chan *RequestError, 50)

	for _, requestData := range rd {
		go f(requestData, params, response, err)
	}

	requests := len(rd)

	var responses []*RequestResponse
	var errors []*RequestError

	for i := 0; i < requests; i++ {
		select {
		case aResp := <-response:
			responses = append(responses, aResp)
		case anErr := <-err:
			errors = append(errors, anErr)
		}
	}

	return responses, errors
}
