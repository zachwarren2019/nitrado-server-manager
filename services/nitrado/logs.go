package nitrado

import (
	"fmt"

	"gitlab.com/BIC_Dev/nitrado-server-manager/services/filestore"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
)

// GetLogsResponse struct
type GetLogsResponse struct {
	PlayerLogs []PlayerLog `json:"player_logs"`
	AdminLogs  []AdminLog  `json:"admin_logs"`
}

// GetLogsError struct
type GetLogsError struct {
	Gameserver filestore.Gameserver
	Error      *utils.ServiceError
}

// PlayerLog struct
type PlayerLog struct {
	Gamertag  string `json:"gamertag"`
	Name      string `json:"name"`
	Message   string `json:"message"`
	Timestamp int64  `json:"timestamp"`
}

// AdminLog struct
type AdminLog struct {
	Name      string `json:"name"`
	Command   string `json:"command"`
	Timestamp int64  `json:"timestamp"`
}

// GetGlobalLogs func
func (n *NitradoRequest) GetGlobalLogs(rd RequestData, params map[string]string, resp chan *RequestResponse, err chan *RequestError) {
	url := fmt.Sprintf("%s/logs/%d", n.Config.NitradoService.BaseURL, rd.Gameserver.ID)

	headers := map[string]string{
		"Service-Token": n.NitradoV2ServiceToken,
		"Account-Name":  rd.Gameserver.AccountName,
	}

	if val, ok := params["cache-control"]; ok {
		headers["Cache-Control"] = val
	}

	var response GetLogsResponse

	rErr := Request(&response, "GET", url, nil, nil, headers)

	if rErr != nil {
		err <- &RequestError{
			Gameserver: rd.Gameserver,
			Error:      rErr,
		}
		return
	}

	resp <- &RequestResponse{
		PlayerLogs: response.PlayerLogs,
		AdminLogs:  response.AdminLogs,
		Gameserver: rd.Gameserver,
	}

	return
}
